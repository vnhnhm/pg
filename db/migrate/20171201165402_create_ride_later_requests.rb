class CreateRideLaterRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :ride_later_requests do |t|
      t.string :customer_name
      t.integer :status

      t.timestamps
    end
  end
end
