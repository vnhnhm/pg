require 'test_helper'

class RideLaterRequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ride_later_request = ride_later_requests(:one)
  end

  test "should get index" do
    get ride_later_requests_url
    assert_response :success
  end

  test "should get new" do
    get new_ride_later_request_url
    assert_response :success
  end

  test "should create ride_later_request" do
    assert_difference('RideLaterRequest.count') do
      post ride_later_requests_url, params: { ride_later_request: { customer_name: @ride_later_request.customer_name, status: @ride_later_request.status } }
    end

    assert_redirected_to ride_later_request_url(RideLaterRequest.last)
  end

  test "should show ride_later_request" do
    get ride_later_request_url(@ride_later_request)
    assert_response :success
  end

  test "should get edit" do
    get edit_ride_later_request_url(@ride_later_request)
    assert_response :success
  end

  test "should update ride_later_request" do
    patch ride_later_request_url(@ride_later_request), params: { ride_later_request: { customer_name: @ride_later_request.customer_name, status: @ride_later_request.status } }
    assert_redirected_to ride_later_request_url(@ride_later_request)
  end

  test "should destroy ride_later_request" do
    assert_difference('RideLaterRequest.count', -1) do
      delete ride_later_request_url(@ride_later_request)
    end

    assert_redirected_to ride_later_requests_url
  end
end
