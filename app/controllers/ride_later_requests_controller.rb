class RideLaterRequestsController < ApplicationController
  before_action :set_ride_later_request, only: [:show, :edit, :update, :destroy]

  # GET /ride_later_requests
  # GET /ride_later_requests.json
  def index
    @ride_later_requests = RideLaterRequest.all
  end

  # GET /ride_later_requests/1
  # GET /ride_later_requests/1.json
  def show
  end

  # GET /ride_later_requests/new
  def new
    @ride_later_request = RideLaterRequest.new
  end

  # GET /ride_later_requests/1/edit
  def edit
  end

  # POST /ride_later_requests
  # POST /ride_later_requests.json
  def create
    @ride_later_request = RideLaterRequest.new(ride_later_request_params)

    respond_to do |format|
      if @ride_later_request.save
        format.html { redirect_to @ride_later_request, notice: 'Ride later request was successfully created.' }
        format.json { render :show, status: :created, location: @ride_later_request }
      else
        format.html { render :new }
        format.json { render json: @ride_later_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ride_later_requests/1
  # PATCH/PUT /ride_later_requests/1.json
  def update
    respond_to do |format|
      if @ride_later_request.update(ride_later_request_params)
        format.html { redirect_to @ride_later_request, notice: 'Ride later request was successfully updated.' }
        format.json { render :show, status: :ok, location: @ride_later_request }
      else
        format.html { render :edit }
        format.json { render json: @ride_later_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ride_later_requests/1
  # DELETE /ride_later_requests/1.json
  def destroy
    @ride_later_request.destroy
    respond_to do |format|
      format.html { redirect_to ride_later_requests_url, notice: 'Ride later request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ride_later_request
      @ride_later_request = RideLaterRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ride_later_request_params
      params.require(:ride_later_request).permit(:customer_name, :status)
    end
end
