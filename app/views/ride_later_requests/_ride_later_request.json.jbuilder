json.extract! ride_later_request, :id, :customer_name, :status, :created_at, :updated_at
json.url ride_later_request_url(ride_later_request, format: :json)
