class RideLaterRequest < ApplicationRecord
  enum status:{
    UnderConstruction:1,
    Submitted: 2,
    Unfulfilled:3,
    QuotesReady:4,
    Fulfilled: 5,
    Rejected:6,
    Ignored:7,
    Cancelled:8,
    Expired:9
  }
end
